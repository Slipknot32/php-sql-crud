
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>CRUD</title>
</head>
<body>

	<?php 
// include 'morceaux/connect.php'; ////connect
// include 'morceaux/clients.php';
// include '';
// include '';
// include '';


// on se connecte à MySQL
	$myPDO = new PDO('mysql:host=localhost;dbname=colyseum', 'mylene');


echo 'Exercice 1: Afficher tous les clients.'. '<br>'. '<br>';

	foreach($myPDO->query('SELECT * FROM clients') as $row) {
		echo $row[firstName].' '.$row[lastName].' '.$row[birthDate]. '<br>';	
	} 

echo 'Exercice 2 : Afficher tous les types de spectacles possibles.'. '<br>'. '<br>';

	foreach($myPDO->query('SELECT * FROM showTypes') as $show) {
		echo $show[type]. '<br>';	
	} 

echo  'Exercice 3: Afficher les 20 premiers clients'. '<br>'. '<br>';

	foreach($myPDO->query('SELECT * FROM clients WHERE id <= 20') as $ids) {
		echo $ids[firstName].' '.$ids[lastName].' '.$ids[birthDate]. '<br>';	
	}


echo "Exercice 4 :N'afficher que les clients possédant une carte de fidélité.". '<br>'. '<br>';

	foreach($myPDO->query('SELECT * FROM clients WHERE card = 1') as $carte) {
		echo $carte[firstName].' '.$carte[lastName].' '.$carte[birthDate].' '.$carte[cardNumber]. '<br>';	
	}

echo "Exercice 5 :Afficher uniquement le nom et le prénom de tous les clients dont le nom commence par la lettre 'M'". '<br>'. '<br>';

//Les afficher comme ceci :
// Nom : *Nom du client*
// Prénom : *Prénom du client*

// Trier les noms par ordre alphabétique.

	foreach($myPDO->query("SELECT * FROM clients WHERE lastName LIKE 'M%' ORDER BY lastName") as $naMe) {
		echo 
		'nom' .': '.$naMe[lastName]. ' --- '.	
		'prenom' .': '. $naMe[firstName]. '<br>';
	}




echo "Exercice 6: Afficher le titre de tous les spectacles ainsi que l'artiste, la date et l'heure. Trier les titres par ordre alphabétique.". '<br>'. '<br>';
// Afficher les résultat comme ceci : *Spectacle* par *artiste*, le *date* à *heure*.

	foreach($myPDO->query('SELECT * FROM shows') as $show) {
		echo $show[title].' '.'par '.' '.$show[performer].', le '.$show[date]. ' à '. $show[startTime].'<br>';	
	} 


echo "Exercice 7 : Afficher tous les clients comme ceci:" . '<br>'. '<br>' ;
// Nom : *Nom de famille du client*
// Prénom : *Prénom du client*
// Date de naissance : *Date de naissance du client*
// Carte de fidélité : *Oui (Si le client en possède une) ou Non (s'il n'en possède pas)*
// Numéro de carte : *Numéro de la carte fidélité du client s'il en possède une.*

foreach($myPDO->query('SELECT * FROM clients order by card' ) as $rows){	
	echo 
	'nom' .': '.$rows[lastName]. ' '.	
	'prenom' .': '. $rows[firstName]. ' - '.
	'Date de naissance'.': '.$rows[birthDate]. ' --- ';
	if($rows["card"] == "1"){
		echo 'Carte de fidélité'.': '. 'oui'.' '.
		'Numéro de carte'. $rows[cardNumber] .'<br>';
	}else{
		echo 'Carte de fidélité: non<br>';
	}
}; 


	?>
</body>
</html>